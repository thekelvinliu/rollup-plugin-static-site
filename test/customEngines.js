import { relative } from 'path';

import test from 'ava';
import ejs from 'ejs';
import fs from 'fs-extra';
import lodashTemplate from 'lodash.template';
import mustache from 'mustache';
import pug from 'pug';

import {
  afterEach,
  beforeEach,
  build,
  linkRegex,
  scriptRegex,
  templatePaths,
} from './_helpers';

test.beforeEach(beforeEach);

test.afterEach(afterEach);

test('call `opts.template.func` with contents of `opts.template.path` and merged `opts.template.data`', async t => {
  t.plan(2);
  const {
    css,
    dir,
    format,
    js,
  } = t.context;
  const moreScripts = [
    'https://www.example.com/lib-a.js',
    'https://www.example.com/lib-b.js',
    'https://www.example.com/test.js',
  ];
  const moreStyles = [
    'https://www.example.com/reset.css',
    'https://www.example.com/framework.css',
    'https://www.example.com/grid.css',
  ];
  const engine = 'pug';
  const path = templatePaths[engine];
  const pathContent = await fs.readFile(path, 'utf8');
  const title = engine;

  const bundle = await build({
    css,
    dir,
    moreScripts,
    moreStyles,
    template: {
      path,
      func(str, data) {
        const scripts = moreScripts.concat(relative(dir, js));
        const styles = moreStyles.concat(relative(dir, css));
        const expectedData = {
          engine,
          scripts,
          styles,
          title,
        };
        t.is(str, pathContent);
        t.deepEqual(data, expectedData);
        return '';
      },
      data: { engine, title },
    },
    title: 'this will be overwritten by `opts.template.data.title`',
  });
  await bundle.write({ format, file: js });
});

test('ejs', async t => {
  t.plan(4);
  const {
    css,
    dir,
    format,
    html,
    js,
  } = t.context;
  const engine = 'ejs';
  const path = templatePaths[engine];
  const title = engine;
  const template = {
    path,
    func: ejs.render,
    data: { engine, title },
  };

  const bundle = await build({ css, dir, template });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  const linkPath = relative(dir, css);
  const scriptPath = relative(dir, js);
  t.regex(output, linkRegex(linkPath));
  t.regex(output, scriptRegex(scriptPath));
  t.regex(output, new RegExp(`<title>${title}</title>`));
  t.regex(output, new RegExp(`<div id="${engine}">`));
});

test('lodash', async t => {
  t.plan(4);
  const {
    css,
    dir,
    format,
    html,
    js,
  } = t.context;
  const engine = 'lodash';
  const path = templatePaths[engine];
  const title = engine;
  const template = {
    path,
    func: (str, data) => lodashTemplate(str)(data),
    data: { engine, title },
  };

  const bundle = await build({ css, dir, template });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  const linkPath = relative(dir, css);
  const scriptPath = relative(dir, js);
  t.regex(output, linkRegex(linkPath));
  t.regex(output, scriptRegex(scriptPath));
  t.regex(output, new RegExp(`<title>${title}</title>`));
  t.regex(output, new RegExp(`<div id="${engine}">`));
});

test('mustache', async t => {
  t.plan(4);
  const {
    css,
    dir,
    format,
    html,
    js,
  } = t.context;
  const engine = 'mustache';
  const path = templatePaths[engine];
  const title = engine;
  const template = {
    path,
    func: mustache.render,
    data: { engine, title },
  };

  const bundle = await build({ css, dir, template });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  const linkPath = relative(dir, css);
  const scriptPath = relative(dir, js);
  t.regex(output, linkRegex(linkPath));
  t.regex(output, scriptRegex(scriptPath));
  t.regex(output, new RegExp(`<title>${title}</title>`));
  t.regex(output, new RegExp(`<div id="${engine}">`));
});

test('pug', async t => {
  t.plan(4);
  const {
    css,
    dir,
    format,
    html,
    js,
  } = t.context;
  const engine = 'pug';
  const path = templatePaths[engine];
  const title = engine;
  const template = {
    path,
    func: pug.render,
    data: { engine, title },
  };

  const bundle = await build({ css, dir, template });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  const linkPath = relative(dir, css);
  const scriptPath = relative(dir, js);
  t.regex(output, linkRegex(linkPath));
  t.regex(output, scriptRegex(scriptPath));
  t.regex(output, new RegExp(`<title>${title}</title>`));
  t.regex(output, new RegExp(`<div id="${engine}">`));
});
