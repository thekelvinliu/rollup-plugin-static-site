# bootstrap
rollup-plugin-static-site example for [bootstrap](http://getbootstrap.com)

## notes
- `yarn build` creates a build in `dist` for production
- `yarn start` watches source files and starts browsersync for development
  - changes to the html template itself doesn't trigger a rebuild
  - however, the next time the bunle is built, the html will also be updated
  - this could potentially be fixed by tweaking the watch settings, but not a huge deal
- adapted from bootstrap's [product example](http://getbootstrap.com/docs/4.1/examples/product/)
- bootstrap css and js (and dependencies) are loaded from cdn
- `src/index.js` references `Holder`, a global defined in `rollup.config.js`
