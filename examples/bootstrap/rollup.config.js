import { join } from 'path';
import browsersync from 'rollup-plugin-browsersync';
import buble from 'rollup-plugin-buble';
import commonjs from 'rollup-plugin-commonjs';
import copy from 'rollup-plugin-copy';
import { eslint } from 'rollup-plugin-eslint';
import filesize from 'rollup-plugin-filesize';
import nodeResolve from 'rollup-plugin-node-resolve';
import postcss from 'rollup-plugin-postcss';
import staticSite from 'rollup-plugin-static-site';

import { main as mainFile } from './package.json';

const IS_DEV = process.env.NODE_ENV === 'development';

const dest = (...args) => join('dist', ...args);
const src = (...args) => join('src', ...args);

const mainPaths = {
  css: {
    dest: dest('css', 'styles.css'),
  },
  html: {
    src: src('template.html'),
  },
  js: {
    dest: dest('js', 'bundle.js'),
    src: mainFile,
  },
};

export default {
  input: mainPaths.js.src,
  output: {
    file: mainPaths.js.dest,
    format: 'iife',
    name: 'js',
    sourcemap: !IS_DEV,
    globals: {
      holderjs: 'Holder',
    },
  },
  plugins: [
    nodeResolve(),
    commonjs(),
    eslint({ include: src('**', '*.js') }),
    buble({ include: src('**', '*.js') }),
    postcss({ extract: IS_DEV ? false : mainPaths.css.dest }),
    copy({
      [src('bootstrap.ico')]: dest('favicon.ico'),
      verbose: true,
    }),
    staticSite({
      css: !IS_DEV && mainPaths.css.dest,
      dir: dest(),
      moreScripts: [
        'https://code.jquery.com/jquery-3.3.1.slim.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js',
        'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.min.js',
      ],
      moreStyles: 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css',
      template: {
        path: mainPaths.html.src,
        data: {
          nav: [
            'tour',
            'product',
            'features',
            'enterprise',
            'support',
            'pricing',
            'cart',
          ],
        },
      },
      title: 'product example for bootstrap',
    }),
    filesize(),
    IS_DEV && browsersync({ server: dest() }),
  ],
  external: ['holderjs'],
  watch: { include: src('**') },
};
