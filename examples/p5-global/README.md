# p5-global
rollup-plugin-static-site example for [p5](https://p5js.org) in [global mode](https://github.com/processing/p5.js/wiki/Global-and-instance-mode)

## notes
- `yarn build` creates a build in `dist` for production
- `yarn start` watches source files and starts browsersync for development
- adapted from [p5.js crash course: recreate art you love](https://blog.kadenze.com/creative-technology/p5-js-crash-course-recreate-art-you-love/)
- p5 via cdnjs
- runs kinda slow for me in firefox, but fine on chrome and safari ¯\_(ツ)_/¯
