import { join } from 'path';

import browsersync from 'rollup-plugin-browsersync';
import buble from 'rollup-plugin-buble';
import { eslint } from 'rollup-plugin-eslint';
import filesize from 'rollup-plugin-filesize';
import postcss from 'rollup-plugin-postcss';
import staticSite from 'rollup-plugin-static-site';

import pkg from './package.json';

const IS_DEV = process.env.NODE_ENV === 'development';

const dest = (...args) => join('dist', ...args);
const src = (...args) => join('src', ...args);

const mainPaths = {
  css: {
    dest: dest('css', 'styles.css'),
  },
  js: {
    dest: dest('js', 'bundle.js'),
    src: pkg.main,
  },
};

export default {
  input: mainPaths.js.src,
  output: {
    file: mainPaths.js.dest,
    format: 'iife',
    name: 'js',
    sourcemap: !IS_DEV,
  },
  plugins: [
    eslint({ include: src('**', '*.js') }),
    buble({ include: src('**', '*.js') }),
    postcss({ extract: IS_DEV ? false : mainPaths.css.dest }),
    staticSite({
      css: !IS_DEV && mainPaths.css.dest,
      dir: dest(),
      moreScripts: 'https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.7.1/p5.min.js',
      title: pkg.name,
    }),
    filesize(),
    IS_DEV && browsersync({ server: dest() }),
  ],
  watch: { include: src('**') },
};
