const getBoundingClientRect = id => document.getElementById(id).getBoundingClientRect();

const colors = {
  bg: [4, 58, 74],
  primary: [253, 174, 120],
  secondary: [226, 129, 161],
};
const maxCircleSize = 20;
const numCols = 16;
const numRows = 10;
const numStrands = 2;
const speed = 0.03;

export const basic = s => {
  const id = 'basic';
  let phase;
  s.setup = () => {
    const { width, height } = getBoundingClientRect(id);
    s.createCanvas(width, height);
    phase = 0;
  };

  s.draw = () => {
    s.background(127);
    const x = s.width / 2;
    const y = s.height / 2 + s.sin(phase) * 50;
    phase = s.frameCount * speed;
    const sizeOffset = (s.cos(phase) + 1) * 0.5;
    const circleSize = sizeOffset * maxCircleSize;
    s.ellipse(x, y, circleSize, circleSize);
  };

  s.windowResized = () => {
    const { width, height } = getBoundingClientRect(id);
    s.resizeCanvas(width, height);
  };
};

export const full = s => {
  const id = 'full';
  let phase;
  s.setup = () => {
    const { width, height } = getBoundingClientRect(id);
    s.createCanvas(width, height);
    s.noStroke();
    phase = 0;
  };

  s.draw = () => {
    phase = s.frameCount * speed;
    s.background(colors.bg);

    for (let strand = 0; strand < numStrands; strand += 1) {
      const strandPhase = phase + s.map(strand, 0, numStrands, 0, s.TWO_PI);

      for (let col = 0; col < numCols; col += 1) {
        const colOffset = s.map(col, 0, numCols, 0, s.TWO_PI);
        const x = s.map(col, 0, numCols, 50, s.width - 50);

        for (let row = 0; row < numRows; row += 1) {
          const y = s.height / 2 + row * 10 + s.sin(strandPhase + colOffset) * 50;
          const sizeOffset = (s.cos(strandPhase - row / numRows + colOffset) + 1) * 0.5;
          const circleSize = sizeOffset * maxCircleSize;

          const mixed = s.lerpColor(
            s.color(colors.primary),
            s.color(colors.secondary),
            row / numRows,
          );
          s.fill(mixed);
          s.ellipse(x, y, circleSize, circleSize);
        }
      }
    }
  };

  s.windowResized = () => {
    const { width, height } = getBoundingClientRect(id);
    s.resizeCanvas(width, height);
  };
};
