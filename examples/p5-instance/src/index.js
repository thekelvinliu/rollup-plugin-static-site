import p5 from 'p5';

import { basic, full } from './sketches';
import './styles.css';

new p5(basic, 'basic');
new p5(full, 'full');
