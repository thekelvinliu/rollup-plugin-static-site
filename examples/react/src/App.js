import React from 'react';

import styles from './App.css';
import logo from './logo.svg';

const App = () => (
  <div className={styles.app}>
    <header className={styles.header}>
      <img
        className={styles.logo}
        alt="logo"
        src={logo}
      />
      <p>
        {'edit '}
        <code>src/App.js</code>
        {' and save to reload.'}
      </p>
      <a
        className={styles.link}
        href="https://reactjs.org"
        rel="noopener noreferrer"
        target="_blank"
      >
        learn react
      </a>
    </header>
  </div>
);

export default App;
