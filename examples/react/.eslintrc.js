module.exports = {
  env: {
    browser: true,
  },
  extends: 'airbnb',
  rules: {
    'arrow-parens': ['error', 'as-needed'],
    'import/no-extraneous-dependencies': [
      'error',
      { optionalDependencies: true },
    ],
    'react/jsx-filename-extension': ['error', { extensions: ['.js', '.jsx'] }],
  },
};
